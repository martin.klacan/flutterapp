import 'dart:async';
import 'package:flutter_subreddit_app/article.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  static const String TABLE_ARTICLES = "articles";
  static const String COLUMN_ID = "id";
  static const String COLUMN_TITLE = "title";
  static const String COLUMN_BODY = "body";
  static const String COLUMN_THUMBNAIL = "thumbnail";
  static const String COLUMN_URL = "url";

  static final DatabaseProvider db = DatabaseProvider._();

  Database _database;

  DatabaseProvider._();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDatabase();
    return _database;
  }

  Future<Database> initDatabase() async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, 'articles.db');

    return await openDatabase(path, version: 1, onCreate: createDatabase);
  }

  Future createDatabase(Database db, int version) async {
    await db.execute('''
CREATE TABLE $TABLE_ARTICLES ( 
  $COLUMN_ID INTEGER PRIMARY KEY, 
  $COLUMN_TITLE TEXT,
  $COLUMN_BODY TEXT,
  $COLUMN_THUMBNAIL TEXT,
  $COLUMN_URL TEXT
  )
''');
  }

  Future<List<Article>> getArticles() async {
    final db = await database;

    var articles = await db.query(TABLE_ARTICLES, columns: [
      COLUMN_ID,
      COLUMN_TITLE,
      COLUMN_BODY,
      COLUMN_THUMBNAIL,
      COLUMN_URL
    ]);

    List<Article> articlesList = List<Article>();

    articles.forEach((currentArticle) {
      Article article = Article.fromMap(currentArticle);

      articlesList.add(article);
    });
    return articlesList;
  }

  Future<Article> insert(Article article) async {
    final db = await database;
    article.id = await db.insert(TABLE_ARTICLES, article.toMap());
    return article;
  }

  Future deleteDatabase() async {
    final db = await database;
    await db.delete(TABLE_ARTICLES);
  }

  Future close() async {
    final db = await database;
    db.close();
  }
}
