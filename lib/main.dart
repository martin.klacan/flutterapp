import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_subreddit_app/data_manager.dart';
import 'package:flutter_subreddit_app/article.dart';
import 'package:flutter_subreddit_app/detail_article_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter subreddit app',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.blueGrey[50],
        primarySwatch: Colors.blue,
      ),
      home: HomePage(title: 'Flutter subreddit app'),
    );
  }
}

class HomePage extends StatefulWidget {

  final String title;

  HomePage({Key key, this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  Future<Null> refreshList() async {
    setState(() {
      _buildListView();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: RefreshIndicator(
          onRefresh: refreshList,
          child: _buildListView(),
        ));
  }

  Widget _buildListView() {

    return FutureBuilder<List<Article>>(
        future: DataManager().getData(),
        builder: (BuildContext context, AsyncSnapshot<List<Article>> snapshot) {

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }

          if (snapshot.hasData) {
            return ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: snapshot.data.length,
              itemBuilder: (_, index) {
                String body = snapshot.data[index].body;
                body = body.substring(0, min(100, body.length));
                String thumbnail = snapshot.data[index].thumbnail;

                return Padding(
                    padding: EdgeInsets.all(8),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DetailArticleView(
                                    article: snapshot.data[index])));
                      },
                      child: Container(
                        padding: EdgeInsets.all(16),
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            Text(
                              snapshot.data[index].title,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                            Text(body),
                            if (thumbnail != "self")
                              Image.network(
                                snapshot.data[index].thumbnail,
                                errorBuilder: (BuildContext context,
                                    Object exception, StackTrace stackTrace) {
                                  return Text(snapshot.data[index].thumbnail);
                                },
                              ),
                          ],
                        ),
                      ),
                    ));
              },
            );
          } else if (snapshot.hasError) {
            return Center(
                child: FloatingActionButton(
              onPressed: () {
                refreshList();
              },
              child: Icon(Icons.refresh),
            ));
          } else
            return Center(child: CircularProgressIndicator());
        });
  }
}
