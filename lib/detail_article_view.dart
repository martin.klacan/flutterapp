import 'package:flutter/material.dart';
import 'package:flutter_subreddit_app/article.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_linkify/flutter_linkify.dart';

class DetailArticleView extends StatelessWidget {

  final Article article;

  DetailArticleView({Key key, @required this.article}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Detail"),
        ),
        body: Container(
            padding: EdgeInsets.all(16),
            child: SingleChildScrollView(
                child: Column(children: <Widget>[
              Text(article.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              Linkify(
                  onOpen: _onOpen,
                  text: article.body,
                  options: LinkifyOptions(humanize: false)),

              if (article.thumbnail != "self")
                Container(
                    child: new InkWell(
                  onTap: () => launch(article.url),
                  child: Image.network(
                    article.thumbnail,
                    errorBuilder: (BuildContext context, Object exception,
                        StackTrace stackTrace) {
                      return Linkify(
                          onOpen: _onOpen,
                          text: article.thumbnail,
                          options: LinkifyOptions(humanize: false));
                    },
                  ),
                ))

            ]))));
  }

  Future<void> _onOpen(LinkableElement link) async {
    if (await canLaunch(link.url)) {
      await launch(link.url);
    } else {
      throw 'Could not launch $link';
    }
  }
}
