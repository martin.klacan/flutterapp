import 'package:dio/dio.dart';
import 'package:flutter_subreddit_app/article.dart';
import 'package:flutter_subreddit_app/database_provider.dart';

class DataManager {
  final String url = 'https://www.reddit.com/r/FlutterDev/hot.json?limit=20';

  Future<List<Article>> getData() async {
    try {
      Response response;
      response = await Dio()
          .get(url, options: Options(responseType: ResponseType.json));
      return dataToObject(response);
    } catch (e) {
      List<Article> dataDB = await DatabaseProvider.db.getArticles();
      if (dataDB.isNotEmpty) return dataDB;
      throw Exception(e);
    }
  }

  List<Article> dataToObject(Response response) {
    List<Article> list = [];
    DatabaseProvider.db.deleteDatabase();
    var len = response.data["data"]["children"].length;

    for (int i = 0; i < len; i++) {
      var data = response.data["data"]["children"][i]["data"];
      int id = i;
      String title = data["title"].toString().trim();
      String body = data["selftext"].toString().trim();
      String thumbnail = data["thumbnail"].toString();
      String url = data["url_overridden_by_dest"].toString();

      if (body.isEmpty) body = " ";
      Article article = new Article(id, title, body, thumbnail, url);
      list.add(article);
      DatabaseProvider.db.insert(article);
    }
    return list;
  }
}
