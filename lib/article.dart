import 'package:flutter_subreddit_app/database_provider.dart';

class Article {
  int id;
  String title;
  String body;
  String thumbnail;
  String url;

  Article(this.id, this.title, this.body, this.thumbnail, this.url);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseProvider.COLUMN_TITLE: title,
      DatabaseProvider.COLUMN_BODY: body,
      DatabaseProvider.COLUMN_THUMBNAIL: thumbnail,
      DatabaseProvider.COLUMN_URL: url
    };

    if (id != null) {
      map[DatabaseProvider.COLUMN_ID] = id;
    }
    return map;
  }

  Article.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseProvider.COLUMN_ID];
    title = map[DatabaseProvider.COLUMN_TITLE];
    body = map[DatabaseProvider.COLUMN_BODY];
    thumbnail = map[DatabaseProvider.COLUMN_THUMBNAIL];
    url = map[DatabaseProvider.COLUMN_URL];
  }
}
